package org.rii.grava;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Test;
import org.rii.grava.domain.Board;
import org.rii.grava.domain.GameState;
import org.rii.grava.domain.Pit;
import org.rii.grava.domain.Player;

/**
 * Created by sergeyr on 4/9/2015.
 */
public class BoardTest {

	private static final int DEFAULT_BOARD_SIZE = 14;
	private static final int DEFAULT_BOARD_INIT_STONES = 3;

	private static final int GRAVA_1_INDEX = DEFAULT_BOARD_SIZE / 2 - 1;
	private static final int GRAVA_2_INDEX = DEFAULT_BOARD_SIZE - 1;
	private static final int RANDOM = 42;

	@Test
	public void testInitState() {
		// Given:
		Board board = new Board();

		// When
		initDefaultBoard(board);

		// Then
		Assert.assertNotNull("Pits are not init", board.getPits());
		assertTrue(board.getPits().size() == DEFAULT_BOARD_SIZE);
		for (Pit pit : board.getPits()) {
			assertEquals("Grava hals initially should be empty", pit.isGrava(), pit.getStones() == 0);
			assertEquals("Regular pits should contain initially default number of stones", !pit.isGrava(),
					pit.getStones() == DEFAULT_BOARD_INIT_STONES);
		}
	}

	@Test
	public void testPutStonesToGrava() throws Exception {
		// Given
		Board board = new Board();
		initDefaultBoard(board);
		board.getPits().set(GRAVA_1_INDEX, new Pit(true, RANDOM, Player.PLAYER_1, GRAVA_1_INDEX));
		board.getPits().set(GRAVA_2_INDEX, new Pit(true, RANDOM, Player.PLAYER_2, GRAVA_2_INDEX));

		// When
		Player player = Player.PLAYER_1;
		callMethod(Board.class, board, "putStonesToGrava", player);

		// Then
		for (Pit pit : board.getPits()) {
			if (!pit.isGrava()) {
				assertEquals("Didn't take player's stones", pit.getOwner() == player, pit.getStones() == 0);
				assertEquals("Took extra stone", pit.getOwner() != player, pit.getStones() == DEFAULT_BOARD_INIT_STONES);
			} else
				assertEquals("Not enourh stones in grava hal!", pit.getOwner() == player, pit.getStones() == RANDOM
						+ DEFAULT_BOARD_INIT_STONES
						* (DEFAULT_BOARD_SIZE - 2) / 2);
		}
	}

	@Test
	public void testIsEmptySideNone() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);

		// When
		boolean isEmptySidePlayer1 = (Boolean) callMethod(Board.class, board, "isEmptySide", Player.PLAYER_1);
		boolean isEmptySidePlayer2 = (Boolean) callMethod(Board.class, board, "isEmptySide", Player.PLAYER_2);

		// Then :
		assertFalse("None of players ran out of stones", isEmptySidePlayer1 || isEmptySidePlayer2);
	}

	@Test
	public void testIsEmptySideOne() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);
		//empty player 1 pits
		for (Pit pit : board.getPits())
			if (pit.getOwner() == Player.PLAYER_1 && !pit.isGrava())
				pit.takeStones();

		// When
		boolean isEmptySidePlayer1 = (Boolean) callMethod(Board.class, board, "isEmptySide", Player.PLAYER_1);
		boolean isEmptySidePlayer2 = (Boolean) callMethod(Board.class, board, "isEmptySide", Player.PLAYER_2);

		// Then :
		assertTrue("Player 1 has no stones", isEmptySidePlayer1);
		assertFalse("Player 2 still has stones", isEmptySidePlayer2);
	}

	@Test
	public void testState() throws Exception {
		// Given
		Board board = new Board();
		initDefaultBoard(board);

		// When: Draw
		for (Pit pit : board.getPits())
			if (pit.isGrava())
				pit.putStones(RANDOM);
			else
				pit.takeStones();

		// Then:
		assertTrue("Should be Draw", board.getState() == GameState.DRAW);

		// When: player 1 win
		initDefaultBoard(board);
		for (Pit pit : board.getPits())
			if (!pit.isGrava() && pit.getOwner() == Player.PLAYER_2)
				pit.takeStones();

		// Then:
		assertTrue("Should be player 1 victory", board.getState() == GameState.PLAYER_1_WIN);

		// When: in progress
		initDefaultBoard(board);

		// Then:
		assertTrue("Should be in progress", board.getState() == GameState.IN_PROGRESS);
	}

	@Test
	public void testMove() throws Exception {
		// Given:
		Board boardBefore = new Board();
		Board boardAfter = new Board();
		initDefaultBoard(boardBefore);
		initDefaultBoard(boardAfter);

		// When:
		int moveIndex = 0;
		Player movePlayer = Player.PLAYER_1;
		boardAfter.move(moveIndex, movePlayer);

		// Then:
		assertTrue("Pit after first move should be empty", boardAfter.getPits().get(moveIndex).getStones() == 0);
		int pitStones = boardBefore.getPits().get(moveIndex).getStones();
		for (int i = 0; i < pitStones; i++) {
			int index = moveIndex + 1 + i;
			assertTrue("Invalid result after move",
					boardBefore.getPits().get(index).getStones() < boardAfter.getPits().get(index).getStones());

		}
	}

	@Test
	public void testCaptureStones() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);
		int moveIndex = 0;
		int stonesToMove = 1;
		board.getPits().set(moveIndex, new Pit(false, stonesToMove, Player.PLAYER_1, moveIndex));
		board.getPits().set(moveIndex + stonesToMove, new Pit(false, 0, Player.PLAYER_1, moveIndex + stonesToMove));

		// When:
		board.move(moveIndex, Player.PLAYER_1);

		// Then:
		int p1GravaStones = board.getPits().get(GRAVA_1_INDEX).getStones();
		int capturedPitStones = ((Pit) callMethod(Board.class, board, "getOppositePit", moveIndex + stonesToMove)).getStones();
		int lastSawedPitStones = board.getPits().get(moveIndex + stonesToMove).getStones();
		assertEquals("should be no stones in captured pit", 0, capturedPitStones);
		assertEquals("Not enough pits in grava hal after capturing", DEFAULT_BOARD_INIT_STONES + 1, p1GravaStones);
		assertEquals("Last sawed pit should be empty", 0, lastSawedPitStones);
	}

	private Object callMethod(Class<?> clazz, Object target, String methName, Object... args)
			throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		Class[] paramTypes = new Class[args.length];
		if (args != null)
			for (int i = 0; i < args.length; i++) {
				paramTypes[i] = args[i].getClass();
			}
		Method processGossipQueueMethod = clazz.getDeclaredMethod(methName, paramTypes);
		processGossipQueueMethod.setAccessible(true);
		return processGossipQueueMethod.invoke(target, args);
	}

	private void initDefaultBoard(Board board) {
		board.setBoardSize(DEFAULT_BOARD_SIZE);
		board.setInitStones(DEFAULT_BOARD_INIT_STONES);
		board.initialize();
	}

}
