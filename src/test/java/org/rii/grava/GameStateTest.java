package org.rii.grava;

import org.junit.Test;
import org.rii.grava.api.GameFlowException;
import org.rii.grava.domain.Board;
import org.rii.grava.domain.Player;

/**
 * Created by sergeyr on 4/14/2015.
 */
public class GameStateTest {
	private static final int DEFAULT_BOARD_SIZE = 14;
	private static final int DEFAULT_BOARD_INIT_STONES = 3;
	private static final int GRAVA_P1_POSITION = DEFAULT_BOARD_SIZE / 2 - 1;

	@Test(expected = GameFlowException.class)
	public void testInvalidMovePosition() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);

		// When:
		board.move(-1, Player.PLAYER_1);

		// Then: exception expected
	}

	@Test(expected = GameFlowException.class)
	public void testMoveGravaHal() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);

		// When:
		board.move(GRAVA_P1_POSITION, Player.PLAYER_1);

		// Then: exception expected
	}

	@Test(expected = GameFlowException.class)
	public void testMoveEmptyPit() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);
		board.getPits().get(0).takeStones();

		// When:
		board.move(0, Player.PLAYER_1);

		// Then: exception expected
	}

	@Test(expected = GameFlowException.class)
	public void testMoveOppositePit() throws Exception {
		// Given:
		Board board = new Board();
		initDefaultBoard(board);

		// When:
		board.move(0, Player.PLAYER_2);

		// Then: exception expected
	}

	private void initDefaultBoard(Board board){
		board.setBoardSize(DEFAULT_BOARD_SIZE);
		board.setInitStones(DEFAULT_BOARD_INIT_STONES);
		board.initialize();
	}
}
