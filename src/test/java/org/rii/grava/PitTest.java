package org.rii.grava;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.rii.grava.domain.Pit;
import org.rii.grava.domain.Player;

/**
 * Created by sergeyr on 4/14/2015.
 */
public class PitTest {
	@Test
	public void testTakeStones() throws Exception {
		//Given :
		int stones = 10;
		Pit pit = new Pit(false, stones, Player.PLAYER_1, 0);

		//When:
		int taken = pit.takeStones();

		//Then:
		assertEquals("Didn't take all stones", stones, taken);
		assertEquals("Pit is not empty", 0, pit.getStones());
	}
}
