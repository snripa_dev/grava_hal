angular.module('gravaApp', [])
    .controller('GravaController', function ($http) {
        // config
        var grava = this;
        grava.move = function (index) {
            grava.errorMessage = '';
            $http.get(
                'http://localhost:8080/move/' + index
            ).success(function (game) {
                    if (game.message) {
                        grava.errorMessage = game.message;
                    } else {
                        grava.pits = game.board;
                        grava.turn = game.turn;
                        grava.gameState = game.state;
                    }
                }).error(function () {
                    // handle errors
                });
        };
        grava.reset = function () {
            grava.turn = "PLAYER_1";
            grava.gameState = "IN_PROGRESS";
            grava.errorMessage = "";
            grava.pits = grava.fetch();
        }

        grava.getClass = function (index, value) {
            var result = "cell";
            //hide extra cells if there are
            if (index < 0 || index > grava.boardSize) return "none";
            // mark grava hal cell
            if ((index + 1) % (grava.boardSize / 2) == 0) result += " grava-hal";
            // player 1/2 cells
            var player1Cell = index < grava.boardSize / 2;
            if (player1Cell) result += " player1"
            else result += " player2";
            // enabled cells
            var playerTurn = player1Cell && grava.turn == "PLAYER_1" || !player1Cell && grava.turn == "PLAYER_2"
            if (playerTurn && value > 0) result += " canMove";
            // shift up 2-nd Grava(
            if (index == grava.boardSize - 1) result += " shiftUp";
            return result;
        }

        grava.fetch = function () {
            $http.get(
                'http://localhost:8080/start'
            ).success(function (game) {
                    grava.boardSize = game.board.length;
                    grava.pits = game.board;
                    grava.turn = game.turn;
                    grava.gameState = game.state;
                }).error(function () {
                    // handle errors
                });
        }
        grava.turn = "PLAYER_1";
        grava.gameState = "IN_PROGRESS";
        grava.pits = [];
        grava.errorMessage = "";
        grava.boardSize = 14; // default
        grava.fetch();
    });