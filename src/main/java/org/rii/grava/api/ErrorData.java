package org.rii.grava.api;

/**
 * Created by sergeyr on 4/13/2015.
 */
public class ErrorData {

	private String message = "SystemError";

	public ErrorData() {
	}

	public ErrorData(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "ErrorData{" +
				"message='" + message + '\'' +
				'}';
	}
}
