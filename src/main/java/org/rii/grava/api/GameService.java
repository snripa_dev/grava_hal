package org.rii.grava.api;

import org.rii.grava.domain.GameModel;

/**
 * Created by sergeyr on 4/13/2015.
 */
public interface GameService {

	GameModel move(Integer pitIndex);

	GameModel resetGame();

}
