package org.rii.grava.api;

/**
 * Created by sergeyr on 4/13/2015.
 */
public class GameFlowException extends RuntimeException {
	public GameFlowException(String message) {
		super(message);
	}
}
