package org.rii.grava.service;

import org.rii.grava.api.ErrorData;
import org.rii.grava.api.GameFlowException;
import org.rii.grava.api.GameService;
import org.rii.grava.domain.GameModel;
import org.rii.grava.domain.GameState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Game controller - handles client requests
 * Created by sergeyr on 4/13/2015.
 */
@RestController
public class GameServiceImpl implements GameService {

	@Autowired
	private GameModel gameModel;

	@Override
	@RequestMapping(value = { "/start", "/reset" }, method = RequestMethod.GET)
	public GameModel resetGame() {
		gameModel.resetState();
		return gameModel;
	}

	@Override
	@RequestMapping(value = { "/move/{index}" }, method = RequestMethod.GET)
	public GameModel move(@PathVariable("index") Integer pitIndex) {
		isGameInProgress();
		gameModel.makeMove(pitIndex);
		return gameModel;
	}

	@ExceptionHandler(GameFlowException.class)
	public ErrorData handleGameException(GameFlowException ex) {
		return new ErrorData(ex.getMessage());
	}

	private void isGameInProgress() {
		if (gameModel.getState() != GameState.IN_PROGRESS)
			throw new GameFlowException("The game is over, no moves allowed");
	}
}
