package org.rii.grava;

import org.rii.grava.api.GameService;
import org.rii.grava.domain.GameModel;
import org.rii.grava.filter.SimpleCORSFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.Filter;
import java.io.PrintStream;

/**
 * Application's entry point
 * Created by sergeyr on 4/13/2015.
 */

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Application.class);
		app.run(args);
	}

	@Configuration
	protected static class ApplicationContextFilterConfiguration {
		@Bean
		public Filter corsFilter() {
			return new SimpleCORSFilter();
		}
	}

}
