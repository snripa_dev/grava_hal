package org.rii.grava.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Reflects the game's current state
 *
 * Created by sergeyr on 4/13/2015.
 */
@Component
public class GameModel implements Serializable {

	private Player turn = Player.PLAYER_1;
	private GameState state = GameState.IN_PROGRESS;
	private Board boardModel;
	private List<Integer> board;

	public void resetState() {
		turn = Player.PLAYER_1;
		state = GameState.IN_PROGRESS;
		boardModel.initialize();
	}

	public Player getTurn() {
		return turn;
	}

	public GameState getState() {
		return state;
	}

	public Board getBoardModel() {
		return boardModel;
	}

	@Autowired
	public void setBoardModel(Board boardModel) {
		this.boardModel = boardModel;
	}

	// for network communication only
	public List<Integer> getBoard() {
		board = boardModel.values();
		return board;
	}

	public void makeMove(int pitIndex) {
		this.turn = boardModel.move(pitIndex, turn);
		state = boardModel.getState();
	}

	@Override
	public String toString() {
		return "GameModel{" +
				"turn=" + turn +
				", board=" + board +
				", state=" + state +
				", boardModel=" + boardModel +
				'}';
	}
}
