package org.rii.grava.domain;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.rii.grava.api.GameFlowException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Grava hal playing board model
 *<p></p>
 *<pre>
 *          Player 1
 *             05 | 04 | 03 | 02 | 01 | 00 |
 * G_H_1(06) |-----------------------------| G_H_2 (13)
 *           | 07 | 08 | 09 | 10 | 11 | 12
 *          Player 2
 *
 *</pre>
 *  <p></p>
 * Created by sergeyr on 4/13/2015.
 */
@Component
public class Board {

	/**
	 * Initial number of stones in each players' pits at the start of the game
	 */
	private int initStones;
	/**
	 * Initial board size including bot players' pits and grava hals
	 */
	private int boardSize;
	private List<Pit> pits = new ArrayList<Pit>();
	private int grava1Position;
	private int grava2Position;

	@PostConstruct
	public void initialize() {
		pits = new ArrayList<Pit>();
		for (int i = 0; i < boardSize; i++) {
			boolean isGrava = (i + 1) % (boardSize / 2) == 0;
			pits.add(new Pit(
					isGrava, /* is Grava hal pit? */
					isGrava ? 0 : initStones, /* start pits with N stones */
					i < boardSize / 2 ? Player.PLAYER_1 : Player.PLAYER_2 /* To which player belongs */
					, i)); /* pit's position */
		}
	}

	@Autowired
	@Value("${org.rii.grava.initStones}")
	public void setInitStones(int initStones) {
		this.initStones = initStones;
	}

	@Autowired
	@Value("${org.rii.grava.boardSize}")
	public void setBoardSize(int boardSize) {
		this.boardSize = boardSize;
		grava1Position = boardSize / 2 - 1;
		grava2Position = boardSize - 1;
	}

	public List<Pit> getPits() {
		return pits;
	}

	public GameState getState() {
		GameState result = GameState.IN_PROGRESS;
		//check if game is over
		boolean noStonesAtPlayer1 = isEmptySide(Player.PLAYER_1);
		boolean noStonesAtPlayer2 = isEmptySide(Player.PLAYER_2);
		boolean isGameOver = noStonesAtPlayer1 || noStonesAtPlayer2;
		if (isGameOver) {
			// put all remaining stones to own pit
			putStonesToGrava(noStonesAtPlayer1 ? Player.PLAYER_2 : Player.PLAYER_1);
			int player1Result = pits.get(grava1Position).getStones();
			int player2Result = pits.get(grava2Position).getStones();
			if (player1Result == player2Result)
				result = GameState.DRAW;
			else
				result = player1Result > player2Result
						? GameState.PLAYER_1_WIN
						: GameState.PLAYER_2_WIN;
		}
		return result;
	}

	public Player move(int index, Player player) {
		verifyMove(index, player);
		// take stones from pit
		int takenStones = pits.get(index).takeStones();
		Pit pitToSaw = null;
		// and saw them
		for (int i = 0; i < takenStones; i++) {
			// take next pit
			pitToSaw = pits.get(++index % boardSize);
			// skip opponent's Grava Hal pit
			if (pitToSaw.isGrava() && pitToSaw.getOwner() != player) {
				pitToSaw = pits.get(++index % boardSize);
			}
			pitToSaw.putStones(1);
		}

		// take another one turn if last stone put in own Grava Hal
		if (pitToSaw.isGrava() && pitToSaw.getOwner() == player)
			return player;

		// capture stones :
		if (pitToSaw.getOwner() == player && pitToSaw.getStones() == 1) {
			int capturedStones = getOppositePit(pitToSaw.getPosition()).takeStones() + pitToSaw.takeStones();
			Pit playersGravaPit = player == Player.PLAYER_1 ? pits.get(grava1Position) : pits.get(grava2Position);
			playersGravaPit.putStones(capturedStones);
		}
		return player.opponent();
	}

	/**
	 *	Get the board pits representation
	 * @return representation of the board as simple array of number of stones in each pit
	 */
	public List<Integer> values() {
		List<Integer> result = new ArrayList<Integer>();
		for (Pit pit : pits)
			result.add(pit.getStones());
		return result;
	}

	private void verifyMove(int index, Player player) throws GameFlowException {
		if (index >= boardSize || index < 0)
			throw new GameFlowException("Invalid pit position : " + index);
		Pit pit = pits.get(index);
		if (pit == null)
			throw new GameFlowException("Invalid pit. Should not happen");
		if (pit.isGrava())
			throw new GameFlowException("Forbidden to take stones from Grava Hal");
		if (pit.getOwner() != player)
			throw new GameFlowException("You can take stones only from your pits");
		if (pit.getStones() == 0)
			throw new GameFlowException("The pit is empty");
	}

	private Pit getOppositePit(Integer position) {
		int oppositePosition = (boardSize - 2) - position;
		return pits.get(oppositePosition);
	}

	private boolean isEmptySide(Player player) {
		boolean emptySide = true;
		int start = player == Player.PLAYER_1 ? 0 : grava1Position + 1;
		int end = player == Player.PLAYER_1 ? grava1Position : grava2Position;
		for (int i = start; i < end; i++) {
			if (pits.get(i).getStones() > 0) {
				emptySide = false;
				break;
			}
		}
		return emptySide;
	}

	private void putStonesToGrava(Player player) {
		int start = player == Player.PLAYER_1 ? 0 : grava1Position + 1;
		int end = player == Player.PLAYER_1 ? grava1Position : grava2Position;
		int gravaPosition = player == Player.PLAYER_1 ? grava1Position : grava2Position;
		for (int i = start; i < end; i++) {
			int stonesToPut = pits.get(i).takeStones();
			pits.get(gravaPosition).putStones(stonesToPut);
		}
	}

	@Override
	public String toString() {
		return "Board{" +
				"pits=" + pits +
				'}';
	}
}
