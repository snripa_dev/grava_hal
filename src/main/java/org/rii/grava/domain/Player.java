package org.rii.grava.domain;

import java.io.Serializable;

/**
 * Created by sergeyr on 4/13/2015.
 */
public enum Player{
	PLAYER_1,
	PLAYER_2;

	public Player opponent() {
		return this == PLAYER_1 ? PLAYER_2 : PLAYER_1;
	}
}
