package org.rii.grava.domain;

import java.io.Serializable;

/**
 * Model of a single pit
 * Created by sergeyr on 4/13/2015.
 */
public class Pit implements Serializable {

	private static final long serialVersionUID = -4941561301716669903L;

	private int stones;
	private final boolean isGrava;
	private final Player owner;
	private final int position;

	public Pit(boolean isGrava, int stones, Player owner, int position) {
		this.isGrava = isGrava;
		this.stones = stones;
		this.owner = owner;
		this.position = position;
	}

	public int getStones() {
		return stones;
	}

	public int takeStones() {
		int result = stones;
		stones = 0;
		return result;
	}

	public void putStones(int stonesToPut) {
		stones += stonesToPut;
	}

	public boolean isGrava() {
		return isGrava;
	}

	public Player getOwner() {
		return owner;
	}

	public int getPosition() {
		return position;
	}

	@Override
	public String toString() {
		return "Pit{" +
				"stones=" + stones +
				", isGrava=" + isGrava +
				", owner=" + owner +
				", position=" + position +
				'}';
	}
}
