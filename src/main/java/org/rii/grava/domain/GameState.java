package org.rii.grava.domain;

/**
 * Created by sergeyr on 4/13/2015.
 */
public enum GameState {
	IN_PROGRESS,
	DRAW,
	PLAYER_1_WIN,
	PLAYER_2_WIN
}
