# README #

This is a Grava Hal game application

### Setting up ###

* make sure java and maven are installed

```
java -version
java version "1.7.0_71"
. . .
mvn -version
Apache Maven 3.2.3
. . .

```

* run mvn clean package in the root of project:

```
mvn clean package
```

* now you should see gravahal-1.0-SNAPSHOT.jar in /target folder. This is ready-to-run executable jar.
* Run jar in order to start server and deploy everything necessary to embedded Tomcat
```
java -jar gravahal-1.0-SNAPSHOT.jar
```
* Follow link in browser:
```
http://localhost:8080/index.html
```


### Technologies used: ###

* backend: Java, Spring Boot
* frontend: angular.js


### Who do I talk to? ###

* please contact [me](mailto:snripa@gmail.com)